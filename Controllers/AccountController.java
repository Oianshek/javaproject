package controllers;

import domain.models.Account;
import services.DepositService;
import services.MainAccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("account")
public class AccountController {
    DepositService deposit = new DepositService();
    MainAccountService main = new MainAccountService();

    @GET
    public String index(){
        return "Hello, world!";
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/mainAcc/create")
    public Response createAccount(Account acc) {
        try {
            main.createAccount(acc);
        }catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.EXPECTATION_FAILED)
                    .entity("This user cannot be created").build();
        }
        return Response.status(Response.Status.CREATED)
                .entity("Created")
                .build();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/mainAcc/replenishment")
    public Response replenishmentMain(Account acc){
        try {
            main.replenishment(acc);
        }catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.EXPECTATION_FAILED)
                    .entity("Impossible to replenish").build();
        }
        return Response.status(Response.Status.ACCEPTED)
                .entity("Successfully replenished!")
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/mainAcc/writeDowns")
    public Response writeDownsMain(Account acc){
        return Response
                .ok(main.writeDowns(acc)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/mainAcc/getInfo")
    public Response getInfoMain(Account acc) {
        Account account;
        try {
            account = main.getInfo(acc);
        } catch (Exception e) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .ok(account).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/deposit/create")
    public Response createDeposit(Account acc){
        return Response
                .ok(deposit.accCheck(acc)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/deposit/replenishment")
    public Response replenishmentDeposit(Account acc){
        try{
            deposit.replenishment(acc);
        }catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.EXPECTATION_FAILED)
                    .entity("Impossible to replenish").build();
        }
        return Response.status(Response.Status.ACCEPTED)
                .entity("Successfully replenished!")
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/deposit/writeDowns")
    public Response writeDownsDeposit(Account acc){
        return Response
                .ok(deposit.writeDowns(acc)).build();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/deposit/getInfo")
    public Response getInfoDeposit(Account acc) {
        Account account;
        try {
            account = deposit.getInfo(acc);
        } catch (Exception e) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .ok(account).build();
    }

}
