package domain.models;

import java.sql.Date;

public class Deposit{
    private long deposit_id;
    private String client_iin;
    private long dep_amount;
    private Date reg_date;
    private int exp_date;

    public Deposit(){

    }

    public Deposit(long deposit_id, long dep_amount, Date reg_date, int exp_date) {
        setDeposit_id(deposit_id);
        setDep_amount(dep_amount);
        setReg_date(reg_date);
        setExp_date(exp_date);
    }


    public long getDeposit_id() {
        return deposit_id;
    }

    public void setDeposit_id(long deposit_id) {
        this.deposit_id = deposit_id;
    }

    public String getClient_iin() {
        return client_iin;
    }

    public void setClient_iin(String client_iin) {
        this.client_iin = client_iin;
    }

    public long getDep_amount() {
        return dep_amount;
    }

    public void setDep_amount(long dep_amount) {
        this.dep_amount = dep_amount;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public int getExp_date() {
        return exp_date;
    }

    public void setExp_date(int exp_date) {
        this.exp_date = exp_date;
    }
}
