package domain.models;

import java.sql.Date;

public class Account{
    private long owner_id;
    private String owner_iin;
    private String fname;
    private String lname;
    private String password;
    private Date reg_date;
    private long money;
    private Deposit dep;

    public Account(){

    }

    public Account(long owner_id, String owner_iin, String fname, String lname, long money_amount, Deposit deposit) {
        setOwner_id(owner_id);
        setOwner_iin(owner_iin);
        setFname(fname);
        setLname(lname);
        setMoney(money_amount);
        setDep(deposit);
    }

    public Account(long owner_id, String owner_iin, String fname, String lname, Date reg_date, long money_amount) {
        setOwner_id(owner_id);
        setOwner_iin(owner_iin);
        setFname(fname);
        setLname(lname);
        setReg_date(reg_date);
        setMoney(money_amount);
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public String getOwner_iin() {
        return owner_iin;
    }

    public void setOwner_iin(String owner_iin) {
        this.owner_iin = owner_iin;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public Deposit getDep() {
        return dep;
    }

    public void setDep(Deposit dep) {
        this.dep = dep;
    }
}