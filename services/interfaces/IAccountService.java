package services.interfaces;

import domain.models.Account;

public interface IAccountService {
    public  void replenishment(Account account);
    public String writeDowns(Account acc);
    public Account getInfo(Account acc);
}
