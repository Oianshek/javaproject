package services;

import domain.models.Account;
import repositories.MainAccountRepo;
import repositories.interfaces.IAccountRepository;
import services.interfaces.IAccountService;

public class MainAccountService implements IAccountService {
    private final IAccountRepository mainRepo = new MainAccountRepo();

    public void createAccount(Account acc){
        mainRepo.create(acc);
    }

    @Override
    public void replenishment(Account account) {
        mainRepo.replenishment(account.getOwner_iin(),account.getMoney()+mainRepo.getMoney(account));
    }

    @Override
    public String writeDowns(Account acc) {

        long maxSum = mainRepo.getMoney(acc);
        if (maxSum - acc.getMoney()<0){
            return "Not enough " +(maxSum - acc.getMoney())+" KZT";
        }
        else{
            maxSum -= acc.getMoney();
            acc.setMoney(maxSum);
            mainRepo.writeDowns(acc.getOwner_iin(),acc.getMoney());
            return "Successfully! Take your money!";
        }
    }

    @Override
    public Account getInfo(Account acc) {
        return mainRepo.getInfo(acc);
    }
}
