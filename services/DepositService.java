package services;

import domain.models.Account;
import repositories.DepositRepository;
import repositories.interfaces.IAccountRepository;
import services.interfaces.IAccountService;


public class DepositService implements IAccountService {

    private final IAccountRepository depRepo = new DepositRepository();
    private DepositRepository depositRepository = new DepositRepository();

    private long lastSum;

    public String accCheck(Account account){
        if(depositRepository.getAccountByIIN(account).equals(null)){
            return "This account does not exist!";
        }else{
            createDeposit(account);
        }
        return null;
    }

    public void createDeposit(Account acc) {
        lastSum = acc.getMoney() + ((acc.getMoney() * acc.getDep().getExp_date()) / 100);
        acc.setMoney(lastSum);
        depRepo.create(acc);
    }

    public void replenishment(Account acc) {
        lastSum = acc.getMoney() + ((acc.getMoney() *  (int)(Math.random() * 5+ 2)/ 100));
        depRepo.replenishment(acc.getOwner_iin(),lastSum);
    }

    @Override
    public String writeDowns(Account acc) {
        long maxSum = depRepo.getMoney(acc);
        if (depRepo.getMoney(acc) - acc.getMoney()<0){
            return "Not enough " +(maxSum - acc.getMoney())+" KZT";
        }
        else{
            maxSum -= (acc.getMoney()+((acc.getMoney()*3)/100));
            acc.setMoney(maxSum);
            depRepo.writeDowns(acc.getOwner_iin(),acc.getMoney());
            return "Successfully! Take your money!";
        }
    }

    @Override
    public Account getInfo(Account acc) {
        return depRepo.getInfo(acc);
    }

}
