package repositories.interfaces;

import domain.models.Account;

public interface IAccountRepository {
    public void create(Account account);
    public  void replenishment(String iin, long money);
    public void writeDowns(String iin, long money);
    public Account getInfo(Account acc);
    public long getMoney(Account acc);
}
