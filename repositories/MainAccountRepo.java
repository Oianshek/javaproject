package repositories;

import domain.models.Account;
import repositories.interfaces.IAccountRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class MainAccountRepo implements IAccountRepository {
    IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void create(Account account) {
        String date1 = String.valueOf(java.time.LocalDate.now());
        LocalDate reg_date = LocalDate.parse(date1);
        try{
            String sql = "INSERT INTO account (owner_iin,fname,lname,password,reg_date,money_amount)" +
                    "VALUES(?, ?, ?, ?, ?, ?)";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, account.getOwner_iin());
            stmt.setString(2,account.getFname());
            stmt.setString(3,account.getLname());
            stmt.setString(4,account.getPassword());
            stmt.setDate(5, Date.valueOf(reg_date));
            stmt.setLong( 6, account.getMoney());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void replenishment(String iin, long money) {
        try{
            String sql = "UPDATE account SET money_amount = ?" +
                    "WHERE owner_iin = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,money);
            stmt.setString(2,iin);

            stmt.execute();
        }catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void writeDowns(String iin, long money) {
        try {
            String sql = "UPDATE  account  SET money_amount = ? " +
                    "WHERE owner_iin = ?";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, money);
            stmt.setString(2, iin);

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public long getMoney(Account acc) {
        try {
            String sql = "SELECT money_amount FROM account WHERE owner_iin = ? ";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,acc.getOwner_iin());

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getLong("money_amount");
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public Account getInfo(Account acc) {
        try {
            String sql = "SELECT owner_id, owner_iin, fname, lname, reg_date, money_amount FROM account " +
                    "WHERE owner_iin=?";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,acc.getOwner_iin() );

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return new Account(
                        rs.getLong("owner_id"),
                        rs.getString("owner_iin"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getDate("reg_date"),
                        rs.getLong("money_amount")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

        return null;
    }
}
