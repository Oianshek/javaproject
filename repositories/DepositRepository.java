package repositories;

import domain.models.Account;
import domain.models.Deposit;
import repositories.interfaces.IAccountRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class DepositRepository implements IAccountRepository {
    IDBRepository dbrepo = new PostgresRepository();
    String date1 = String.valueOf(java.time.LocalDate.now());
    LocalDate reg_date = LocalDate.parse(date1);

    @Override
    public void create(Account account) {

        try {
            String sql = "INSERT INTO deposits (client_iin, dep_amount, reg_date, exp_date) " +
                    "VALUES(?, ?, ?, ?)";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, account.getOwner_iin());
            stmt.setLong( 2, account.getMoney());
            stmt.setDate(3, Date.valueOf(reg_date));
            stmt.setInt(4, account.getDep().getExp_date());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void replenishment(String iin, long money) {
        try{
            String sql = "UPDATE deposits SET dep_amount = ?" +
                    "WHERE client_iin = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,money);
            stmt.setString(2,iin);

            stmt.execute();
        }catch (SQLException ex){
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }


    @Override
    public void writeDowns(String iin, long money) {
        try {
            String sql = "UPDATE  deposits  SET dep_amount = ? " +
                    "WHERE client_iin = ?";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, money);
            stmt.setString(2, iin);

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public Account getInfo(Account acc) {

        try {
            String sql = "SELECT deposits.deposit_id, deposits.dep_amount,deposits.reg_date, deposits.exp_date," +
                    "account.owner_id, account.owner_iin, account.fname, account.lname, account.money_amount " +
                    "FROM deposits " +
                    "INNER JOIN account on deposits.client_iin = account.owner_iin " +
                    "WHERE deposits.client_iin= ?";

            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, acc.getOwner_iin());

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return new Account(
                        rs.getLong("owner_id"),
                        rs.getString("owner_iin"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getLong("money_amount"),
                        new Deposit(
                                rs.getLong("deposit_id"),
                                rs.getLong("dep_amount"),
                                rs.getDate("reg_date"),
                                rs.getInt("exp_date")
                        )
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public long getMoney(Account account) {
        try {
            String sql = "SELECT dep_amount FROM deposits WHERE client_iin = ? ";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,account.getOwner_iin());

            stmt.execute();

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getLong("dep_amount");
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return 0;
    }

    public String getAccountByIIN(Account account){
        try {
            String sql = "SELECT owner_iin FROM account WHERE owner_iin = ? ";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,account.getOwner_iin());

            stmt.execute();

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getString("owner_iin");
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }
}
